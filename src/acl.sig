(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Per-user access control lists for resources various *)

signature ACL = sig

    type acl = {user : string,  (* The UNIX user being granted a permission *)
		class : string, (* The type of permission granted *)
		value : string} (* The object for which it is granted *)

    val query : acl -> bool
    (* Is this permission granted? *)

    val queryAll : string -> (string * string list) list
    (* What are all of a user's permissions, by class? *)

    val users : unit -> string list
    (* Which users have been granted privileges? *)

    val whoHas : {class : string, value : string} -> string list
    (* Which users have a permission? *)

    val class : {user : string, class : string} -> DataStructures.StringSet.set
    (* For what objects does the user have the permission? *)

    val grant : acl -> unit
    val revoke : acl -> unit
    (* Grant/ungrant the user the permission. *)

    val revokeFromAll : {class : string, value : string} -> unit

    val rmuser : string -> unit
    (* Remove all of a user's privileges. *)

    val read : string -> unit
    val write : string -> unit
    (* Read/write saved ACL state from/to a file *)

end
