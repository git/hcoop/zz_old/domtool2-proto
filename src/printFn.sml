(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Pretty-printing Domtool configuration file ASTs *)

functor PrintFn (PF : PRINTFN_INPUT) :> PRINTFN_OUTPUT where type rendering = PF.rendering = struct

open Ast PF
open PD

fun dBox ds = hovBox (PPS.Rel 1, ds)
fun dvBox ds = vBox (PPS.Rel 0, ds)
fun ivBox ds = vBox (PPS.Rel 1, ds)

fun modify file =
    let
	val file' = #file (OS.Path.splitDirFile file)
	val file' = #base (OS.Path.splitBaseExt file')
    in
	file'
    end

fun parenIf pn ds =
    if pn then
	dBox (punct "(" :: ds @ [punct ")"])
    else
	dBox ds

fun p_pred' pn (p, _) =
    case p of
	CRoot => keyword "Root"
      | CConst s => context s
      | CPrefix p => dBox [punct "^", p_pred' true p]
      | CNot p => dBox [punct "!", p_pred' true p]
      | CAnd (p1, p2) =>
	parenIf pn [p_pred' true p1, space 1, punct "&", space 1, p_pred' true p2]

val p_pred = p_pred' false

fun p_predBoxed p = dBox [punct "[", p_pred p, punct "]"]

fun p_typ' pn (t, _) =
    case t of
	TBase s => typ s
      | TList t => dBox [punct "[", p_typ' false t, punct "]"]
      | TArrow (t1, t2) =>
	parenIf pn [p_typ' true t1, space 1, punct "->", space 1, p_typ' false t2]
      | TAction (p, r1, r2) =>
	(case (StringMap.numItems r1, StringMap.numItems r2) of
	     (0, 0) => parenIf pn [p_predBoxed p]
	   | (_, 0) => 	parenIf pn [p_predBoxed p, space 1, p_record r1]
	   | _ => parenIf pn [p_predBoxed p, space 1, p_record r1, space 1,
			      punct "=>", space 1, p_record r2])
      | TNested (p, t) =>
	parenIf pn [p_pred' false p, space 1, punct "=>", space 1, p_typ' false t]

      | TError => keyword "<error>"
      | TUnif (_, ref (SOME t)) => p_typ' pn t
      | TUnif (name, ref NONE) => string ("<" ^ name ^ ">")

and p_record r =
    case StringMap.foldri (fn (name, t, d) =>
			      SOME (case d of
					NONE => dBox [field name, space 1,
						      punct ":", space 1, p_typ t]
				      | SOME d => dBox [dBox [field name, space 1,
							      punct ":", space 1, p_typ t],
							punct ",", space 1, d]))
			  NONE r of
	NONE => punct "{}"
      | SOME d => dBox [punct "{", d, punct "}"]

and p_typ t = p_typ' false t

fun p_exp' pn (e, _) =
    case e of
	EInt n => lit (Int.toString n)
      | EString s => lit (String.concat ["\"", String.toString s, "\""])
      | EList es =>
	(case foldr (fn (e, d) =>
			SOME (case d of
				  NONE => p_exp e
				| SOME d => dBox [p_exp e, punct ",", space 1, d]))
		    NONE es of
	     NONE => punct "[]"
	   | SOME d => dBox [punct "[", d, punct "]"])

      | ELam (x, NONE, e) => dBox [punct "(\\", space 1, exp x, space 1,
				punct "->", space 1, p_exp e, punct ")"]
      | ELam (x, SOME t, e) => dBox [punct "(\\", space 1, exp x, space 1,
				punct ":", space 1,
				dBox [punct "(", p_typ t, punct ")"],
				space 1, punct "->", space 1, p_exp e, punct ")"]
      | EALam (x, p, e) => dBox [punct "(\\", space 1, exp x, space 1,
				 punct ":", space 1, p_pred p, 
				 space 1, punct "->", space 1, p_exp e, punct ")"]

      | EVar x => exp x
      | EApp (e1, e2) => parenIf pn [p_exp e1, break {nsp = 1, offset = 0}, p_exp' true e2]

      | ESkip => keyword "_"
      | ESet (x, e) => parenIf pn [exp x, space 1, punct "=", space 1, p_exp e]
      | EGet (x1, NONE, x2, e) => parenIf pn [dBox [exp x1, space 1, punct "<-",
						    space 1, exp x2, punct ";", space 1],
					      p_exp e]
      | EGet (x1, SOME t, x2, e) => parenIf pn [dBox [exp x1, space 1, punct ":", space 1, p_typ t,
						      space 1, punct "<-",
						      space 1, exp x2, punct ";", space 1],
						p_exp e]
      | ESeq es => parenIf pn (valOf (foldr (fn (e, NONE) => SOME [p_exp e]
					| (e, SOME ds) => SOME (dBox [p_exp e, punct ";", newline] :: ds))
				      NONE es))
      | ELocal (e1, e2) => dBox [keyword "let", space 1,
				 p_exp e1, space 1,
				 keyword "in", space 1,
				 p_exp e2, space 1,
				 keyword "end"]
      | EWith (e1, (ESkip, _)) => dBox [p_exp e1, space 1, keyword "with", space 1, keyword "end"]
      | EWith (e1, e2) => dBox [p_exp e1, space 1, keyword "with", p_exp e2, space 1, keyword "end"]
      | EIf (e1, e2, e3) => dBox [keyword "if", space 1, p_exp e1,
				  space 1, keyword "then", space 1, p_exp e2,
				  space 1, keyword "else", space 1, p_exp e3]
and p_exp e = p_exp' false e

fun p_decl d =
    case d of
	DExternType name => anchor ("T_" ^ name,
				    dBox [keyword "extern", space 1,
					  keyword "type", space 1,
					  ident name])
      | DExternVal (name, t) => anchor ("V_" ^ name,
					dBox [keyword "extern", space 1,
					      keyword "val", space 1,
					      ident name, space 1,
					      string ":", space 1,
					      p_typ t])
      | DVal (name, NONE, _) => string "Unannotated val declaration!"
      | DVal (name, SOME t, _) => anchor ("V_" ^ name,
					  dBox [keyword "val", space 1,
						ident name, space 1,
						punct ":", space 1,
						p_typ t])
      | DContext name => anchor ("C_" ^ name,
				 dBox [keyword "context", space 1,
				       ident name])

fun p_decl_fref d =
    case d of
	DExternType name => dBox [keyword "extern", space 1,
				  keyword "type", space 1,
				  link ("#T_" ^ name, ident name)]
      | DExternVal (name, t) => dBox [keyword "extern", space 1,
				      keyword "val", space 1,
				      link ("#V_" ^ name, ident name),
				      space 1,
				      string ":", space 1,
				      p_typ t]
      | DVal (name, NONE, _) => string "Unannotated val declaration!"
      | DVal (name, SOME t, _) => dBox [keyword "val", space 1,
					link ("#V_" ^ name, ident name),
					space 1,
					punct ":", space 1,
					p_typ t]
      | DContext name => dBox [keyword "context", space 1,
			       link ("#C_" ^ name, ident name)]

fun output d =
    let
	val myStream = openStream ()
    in
	description (myStream, d);
	PPS.flushStream myStream;
	closeStream myStream
    end

fun preface (s, d) = output (PD.hovBox (PD.PPS.Rel 0,
					[PD.string s, PD.space 1, d]))

end
