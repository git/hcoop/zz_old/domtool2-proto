(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Main interface *)

signature MAIN = sig

    val init : unit -> unit
    val setupUser : unit -> string

    val check : string -> Env.env * Ast.exp option
    val check' : Env.env -> string -> Env.env
    val checkDir : string -> unit

    val basis : unit -> Env.env

    val reduce : string -> Ast.exp option
    val eval : string -> unit

    val request : string -> unit
    val requestDir : string -> unit

    val requestPing : unit -> OS.Process.status
    val requestShutdown : unit -> unit
    val requestGrant : Acl.acl -> unit
    val requestRevoke : Acl.acl -> unit
    val requestListPerms : string -> (string * string list) list option
    val requestWhoHas : {class : string, value : string} -> string list option
    val requestRmdom : string list -> unit
    val requestRegen : unit -> unit
    val requestRegenTc : unit -> unit
    val requestRmuser : string -> unit
    val requestDescribe : string -> unit

    val requestSlavePing : unit -> OS.Process.status
    val requestSlaveShutdown : unit -> unit

    val service : unit -> unit
    val slave : unit -> unit

    val listBasis : unit -> string list
    val autodocBasis : string -> unit

    val requestDbUser : {dbtype : string, passwd : string option} -> unit
    val requestDbPasswd : {dbtype : string, passwd : string} -> unit
    val requestDbTable : {dbtype : string, dbname : string} -> unit
    val requestDbDrop : {dbtype : string, dbname : string} -> unit
    val requestDbGrant : {dbtype : string, dbname : string} -> unit

    val requestListMailboxes : string -> Vmail.listing
    val requestNewMailbox : {domain : string, user : string,
			     passwd : string, mailbox : string} -> unit
    val requestPasswdMailbox : {domain : string, user : string, passwd : string}
			       -> unit
    val requestRmMailbox : {domain : string, user : string} -> unit

    val requestSaQuery : string -> unit
    val requestSaSet : string * bool -> unit

    val requestSmtpLog : string -> unit

    val requestMysqlFixperms : unit -> unit

    val requestApt : {node : string, pkg : string} -> OS.Process.status
    val requestCron : {node : string, uname : string} -> OS.Process.status
    val requestFtp : {node : string, uname : string} -> OS.Process.status
    val requestTrustedPath : {node : string, uname : string} -> OS.Process.status
    val requestSocketPerm : {node : string, uname : string} -> OS.Process.status
    val requestFirewall : {node : string, uname : string} -> OS.Process.status
end
