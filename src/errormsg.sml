(* This file comes mostly from "Modern Compiler Implementation in ML," by Andrew Appel
 * http://www.cs.princeton.edu/~appel/modern/ml/
 *)

structure ErrorMsg :> ERRORMSG =
  struct
    (* Initial values of compiler state variables *)
    val anyErrors = ref false
    val anyWarnings = ref false
    val errorText = ref ""
    val fileName = ref ""
    val lineNum = ref 1
    val linePos = ref [1]
    val sourceStream = ref TextIO.stdIn

    fun print msg = (errorText := !errorText ^ msg;
		     TextIO.print msg)

    (* Reset compiler to initial state *)
    fun reset() = (anyErrors:=false;
		   anyWarnings:=false;
		   errorText:="";
                   fileName:="";
                   lineNum:=1;
                   linePos:=[1];
                   sourceStream:=TextIO.stdIn)

    fun notify f prefix posopt (msg:string) =
      let
        val (startpos, endpos) = Option.getOpt (posopt, (0, 0))
        fun look(pos,a::rest,n) =
          if a<pos then app print [Int.toString n,
                                   ".",
                                   Int.toString (pos-a)]
          else look(pos,rest,n-1)
          | look _ = print "0.0"
      in
        f ();
        print (!fileName); print ":";
        look(startpos, !linePos, !lineNum);
        if startpos=endpos then () else (print "-"; look(endpos, !linePos, !lineNum));
        app print [":", prefix, ": ", msg, "\n"]
      end

    val error = notify (fn () => anyErrors := true) "error"
    val warning = notify (fn () => anyWarnings := true) "warning"

      val dummyLoc = (0, 0)

      exception Error
  end

