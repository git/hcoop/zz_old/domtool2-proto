(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for dbtool *)

val _ =
    case CommandLine.arguments () of
	[] => print "Invalid command-line arguments\n"
      | dbtype :: rest =>
	case Dbms.lookup dbtype of
	    NONE => print ("Unknown database type " ^ dbtype ^ ".\n")
	  | SOME {getpass, ...} =>
	    case rest of
		["adduser"] =>
		let
		    val pass = case getpass of
				   NONE => SOME NONE
				 | SOME f =>
				   case f () of
				       Client.Passwd pass => SOME (SOME pass)
				     | Client.Aborted => SOME NONE
				     | Client.Error => NONE
		in
		    case pass of
			NONE => ()
		      | SOME pass => Main.requestDbUser {dbtype = dbtype, passwd = pass}
		end
	      | ["passwd"] =>
		let
		    val pass = case getpass of
				   NONE => NONE
				 | SOME f =>
				   case f () of
				       Client.Passwd pass => SOME pass
				     | _ => NONE
		in
		    case pass of
			NONE => ()
		      | SOME pass => Main.requestDbPasswd {dbtype = dbtype, passwd = pass}
		end
	      | ["createdb", dbname] =>
		if Dbms.validDbname dbname then
		    Main.requestDbTable {dbtype = dbtype, dbname = dbname}
		else
		    print ("Invalid database name " ^ dbname ^ ".\n")
	      | ["dropdb", dbname] =>
		if Dbms.validDbname dbname then
		    Main.requestDbDrop {dbtype = dbtype, dbname = dbname}
		else
		    print ("Invalid database name " ^ dbname ^ ".\n")
	      | ["grant", dbname] =>
		if Dbms.validDbname dbname then
		    Main.requestDbGrant {dbtype = dbtype, dbname = dbname}
		else
		    print ("Invalid database name " ^ dbname ^ ".\n")
	      | _ => print "Invalid command-line arguments\n"
