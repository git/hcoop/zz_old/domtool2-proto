(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Domtool configuration language type checking *)

signature TYCHECK = sig

    val checkTyp : Env.env -> Ast.typ -> Ast.typ

    val checkExp : Env.env -> Ast.exp -> Ast.typ
    val checkUnit : Env.env -> Ast.exp -> Ast.typ
    (* [checkUnit] checks that all unification variables have been resolved. *)

    val checkDecl : Env.env -> Ast.decl -> Env.env

    val checkFile : Env.env -> Ast.typ -> Ast.file -> Env.env

    val resetUnif : unit -> unit
    val newUnif : unit -> Ast.typ'

    val preface : string * Print.PD.pp_desc -> unit

    val allowExterns : unit -> unit
    val disallowExterns : unit -> unit

end
