(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* OpenSSL *)

signature OPENSSL = sig

exception OpenSSL of string

val ssl_err : string -> unit

type context
type bio
type listener

val readChar : bio -> char option
val readInt : bio -> int option
val readLen : bio * int -> string option
val readChunk : bio -> string option
val readString : bio -> string option

val writeChar : bio * char -> unit
val writeInt : bio * int -> unit
val writeString' : bio * string -> unit
val writeString : bio * string -> unit

val context : bool -> string * string * string -> context

val connect : context * string -> bio
val close : bio -> unit

val listen : context * int -> listener
val shutdown : listener -> unit

val accept : listener -> bio option

val peerCN : bio -> string

end
