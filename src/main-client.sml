(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for configuration requests *)

fun uid () =
    case Posix.ProcEnv.getenv "DOMTOOL_USER" of
	NONE => Posix.ProcEnv.getuid ()
      | SOME user => Posix.SysDB.Passwd.uid (Posix.SysDB.getpwnam user)

fun domtoolRoot () =
    let
	val dname = Posix.SysDB.Passwd.home (Posix.SysDB.getpwuid (uid ()))
    in
	OS.Path.joinDirFile {dir = dname,
			     file = ".domtool"}
    end

val (doit, doitDir, args) =
    case CommandLine.arguments () of
	"-tc" :: args => (fn fname => (Main.setupUser (); ignore (Main.check fname)),
			  Main.checkDir,
			  args)
      | args => (Main.request,
		 Main.requestDir,
		 args)

val _ =
    case args of
	[fname] =>
	if Posix.FileSys.access (fname, []) then
	    doit fname
	else
	    doit (OS.Path.joinDirFile {dir = domtoolRoot (),
				       file = fname})
      | [] => doitDir (domtoolRoot ())
      | _ => print "Invalid command-line arguments\n"
