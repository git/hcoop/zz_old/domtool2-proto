(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Useful HCoop shortcuts *)

structure HCoop :> HCOOP = struct

open Ast

val dl = ErrorMsg.dummyLoc

val _ = Env.type_one "homedir_path"
	Env.string
	(fn dir => Domain.yourPath (OS.Path.concat (Domain.homedir (), dir))
		   handle _ => false)

fun addHome name = Env.registerFunction (name,
				      fn [(EString dir, _)] => (SOME (EString (OS.Path.concat (Domain.homedir (), dir)), dl)
								handle _ => NONE)
				       | _ => NONE)

val () = addHome "home"
val () = addHome "homeS"

end
