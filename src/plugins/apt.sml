(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* APT package database querying *)

structure Apt :> APT = struct

fun validName s = CharVector.all (fn ch => Char.isAlphaNum ch orelse ch = #"_" orelse ch = #"-" orelse ch = #".") s
		  andalso (size s > 0 andalso String.sub (s, 0) <> #"-")
			   
fun installed name =
    validName name
    andalso let
	val proc = Unix.execute ("/usr/bin/apt-cache", ["policy", name])
        val inf = Unix.textInstreamOf proc

	val _ = TextIO.inputLine inf
    in
	(case TextIO.inputLine inf of
	     NONE => false
	   | SOME line =>
	     case String.tokens Char.isSpace line of
		 [_, "(none)"] => false
	       | [_, _] => true
	       | _ => false)
	before ignore (Unix.reap proc)
    end

end
