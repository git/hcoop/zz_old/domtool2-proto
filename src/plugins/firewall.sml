(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Firewall rule querying *)

structure Firewall :> FIREWALL = struct

fun query uname =
    let
        val inf = TextIO.openIn "/etc/firewall/users.rules"

        fun loop rules =
            case TextIO.inputLine inf of
                NONE => List.rev rules
              | SOME line =>
                if String.sub (line, 0) = #"#" then
                    loop rules
                else case String.tokens Char.isSpace line of
			 uname'::rest =>
			 if uname = uname' then
                             loop (String.concatWith " " rest :: rules)
			 else
                             loop rules
                       | _ => loop rules
    in
        loop []
        before TextIO.closeIn inf
    end handle IO.Io _ => []


end
