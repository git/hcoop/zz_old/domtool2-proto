(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* PostgreSQL user/table management *)

structure Postgres :> POSTGRES = struct

fun adduser {user, passwd} =
    Option.map (fn s => "Error executing CREATE USER script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.adduser, user])

fun passwd _ = SOME "We don't use PostgreSQL passwords."

fun createdb {user, dbname} =
    Option.map (fn s => "Error executing CREATE DATABASE script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.createdb, user, " ", dbname])

fun dropdb {user, dbname} =
    Option.map (fn s => "Error executing DROP DATABASE script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.dropdb, user, " ", dbname])

val _ = Dbms.register ("postgres", {getpass = NONE,
				    adduser = adduser,
				    passwd = passwd,
				    createdb = createdb,
				    dropdb = dropdb,
				    grant = fn _ => SOME "You don't need to use GRANT for Postgres."})

end
