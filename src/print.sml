(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Pretty-printing Domtool configuration file ASTs *)

signature PRINT_ARG = PRINTFN_INPUT where type rendering = unit

structure PrintArg :> PRINT_ARG = struct

structure SM = TextIOPP

structure PD = PPDescFn(SM)
open PD

val keyword = string
val punct = string
val field = string
val lit = string
val ident = string

val context = string
val typ = string
val exp = string

fun anchor (_, d) = d
fun link (_, d) = d

type rendering = unit
fun openStream () = SM.openOut {dst = TextIO.stdOut, wid = 80}
fun closeStream s = (SM.newline s; SM.closeStream s)

end

structure Print = PrintFn(PrintArg)
