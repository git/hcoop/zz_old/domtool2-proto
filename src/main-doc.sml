(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for documentation generation *)

fun processArgs (args, basis, outdir, files, emacs) =
    case args of
	[] => (basis, outdir, files, emacs)
      | "-basis" :: rest => processArgs (rest, true, outdir, files, emacs)
      | "-out" :: dir :: rest => processArgs (rest, basis, dir, files, emacs)
      | "-emacs" :: rest => processArgs (rest, basis, outdir, files, true)
      | file :: rest =>
	if size file > 0 andalso String.sub (file, 0) <> #"-" then
	    processArgs (rest, basis, outdir, file :: files, emacs)
	else
	    raise Fail ("Uknown switch " ^ file)

val _ =
    let
	val (basis, outdir, files, emacs) = processArgs (CommandLine.arguments (),
							 false,
							 OS.FileSys.getDir (),
							 [],
							 false)

	val files = if basis then
			Main.listBasis () @ files
		    else
			files
    in
	Tycheck.allowExterns ();
	if emacs then
	    Autodoc.makeEmacsKeywords files
	else
	    Autodoc.autodoc {outdir = outdir, infiles = files}
    end
