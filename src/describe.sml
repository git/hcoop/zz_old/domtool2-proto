(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Error message generation *)

structure Describe :> DESCRIBE = struct

open Ast Print

structure SM = StringMap

exception UnequalDomains
	  
fun eqRecord f (r1, r2) =
    (SM.appi (fn (k, v1) =>
		 case SM.find (r2, k) of
		     NONE => raise UnequalDomains
		   | SOME v2 =>
		     if f (v1, v2) then
			 ()
		     else
			 raise UnequalDomains) r1;
     SM.appi (fn (k, v2) =>
		 case SM.find (r1, k) of
		     NONE => raise UnequalDomains
		   | SOME v1 =>
		     if f (v1, v2) then
			 ()
		     else
			 raise UnequalDomains) r2;
     true)
    handle UnequalDomains => false

fun eqPred ((p1, _), (p2, _)) =
    case (p1, p2) of
	(CRoot, CRoot) => true
      | (CConst s1, CConst s2) => s1 = s2
      | (CPrefix p1, CPrefix p2) => eqPred (p1, p2)
      | (CNot p1, CNot p2) => eqPred (p1, p2)
      | (CAnd (p1, q1), CAnd (p2, q2)) =>
	eqPred (p1, p2) andalso eqPred (q1, q2)

      | _ => false

fun eqTy (t1All as (t1, _), t2All as (t2, _)) =
    case (t1, t2) of
	(TBase s1, TBase s2) => s1 = s2
      | (TList t1, TList t2) => eqTy (t1, t2)
      | (TArrow (d1, r1), TArrow (d2, r2)) =>
	eqTy (d1, d2) andalso eqTy (r1, r2)

      | (TAction (p1, d1, r1), TAction (p2, d2, r2)) =>
	eqPred (p1, p2) andalso eqRecord eqTy (d1, d2)
	andalso eqRecord eqTy (r1, r2)

      | (TNested (p1, q1), TNested (p2, q2)) =>
	eqPred (p1, p2) andalso eqTy (q1, q2)

      | (TUnif (_, ref (SOME t1)), _) => eqTy (t1, t2All)
      | (_, TUnif (_, ref (SOME t2))) => eqTy (t1All, t2)

      | (TUnif (_, r1), TUnif (_, r2)) => r1 = r2

      | (TError, TError) => true

      | _ => false

fun describe_unification_error t ue =
    case ue of
	UnifyPred (p1, p2) =>
	(print "Reason: Incompatible contexts.\n";
	 preface ("Have:", p_pred p1);
	 preface ("Need:", p_pred p2))
      | UnifyTyp (t1, t2) =>
	if eqTy (t, t1) then
	    ()
	else
	    (print "Reason: Incompatible types.\n";
	     preface ("Have:", p_typ t1);
	     preface ("Need:", p_typ t2))
      | UnifyOccurs (name, t') =>
	if eqTy (t, t') then
	    ()
	else
	    (print "Reason: Occurs check failed for ";
	     print name;
	     print " in:\n";
	     output (p_typ t))

fun will_be_action (t, _) =
    case t of
	TArrow (_, t') => will_be_action t'
      | TAction _ => true
      | TNested _ => true
      | TUnif (_, ref (SOME t')) => will_be_action t'
      | _ => false

fun get_first_arg (t, _) =
    case t of
	TArrow (t', _) => t'
      | TUnif (_, ref (SOME t')) => get_first_arg t'
      | _ => raise Fail "get_first_arg failed!"

fun hint te =
    case te of
	WrongType (_, _, (TBase "string", _), (TBase "your_domain", _), _) =>
	SOME "Did you forget to request permission to configure this domain?  See:\n\thttps://members.hcoop.net/portal/domain"
      | WrongType (_, (EString dom, _), (TBase "string", _), (TBase "domain", _), _) =>
	if CharVector.exists Char.isUpper dom then
	    SOME "Uppercase letters aren't allowed in domain strings."
	else
	    NONE
      | _ => NONE

fun describe_type_error' loc te =
    case te of
	WrongType (place, e, t1, t2, ueo) =>
	(ErrorMsg.error (SOME loc) (place ^ " has wrong type.");
	 preface (" Expression:", p_exp e);
	 preface ("Actual type:", p_typ t1);
	 preface ("Needed type:", p_typ t2);
	 Option.app (describe_unification_error t1) ueo)
      |	WrongForm (place, form, e, t, ueo) =>
	if form = "action" andalso will_be_action t then
	    (ErrorMsg.error (SOME loc) "Not enough arguments passed to configuration function.";
	     preface (" Expression so far:", p_exp e);
	     preface ("Next argument type:", p_typ (get_first_arg t)))
	else
	    (ErrorMsg.error (SOME loc) (place ^ " has a non-" ^ form ^ " type.");
	     preface ("Expression:", p_exp e);
	     preface ("      Type:", p_typ t);
	     Option.app (describe_unification_error t) ueo)
      | UnboundVariable name =>
	ErrorMsg.error (SOME loc) ("Unbound variable " ^ name ^ ".\n")
      |	WrongPred (place, p1, p2) =>
	(ErrorMsg.error (SOME loc) ("Context incompatibility for " ^ place ^ ".");
	 preface ("Have:", p_pred p1);
	 preface ("Need:", p_pred p2))

fun ununify (tAll as (t, _)) =
    case t of
	TUnif (_, ref (SOME t)) => ununify t
      | _ => tAll

fun normalize_error err =
    case err of
	WrongType (s, e, t1, t2, ueo) =>
	WrongType (s, e, ununify t1, ununify t2, ueo)
      | WrongForm (s1, s2, e, t, ueo) =>
	WrongForm (s1, s2, e, ununify t, ueo)
      | UnboundVariable _ => err
      | WrongPred _ => err

fun describe_type_error loc te =
    let
	val te = normalize_error te
    in
	describe_type_error' loc te;
	Option.app (fn s => (print "Hint Monster says:\n";
			     print s;
			     print "\n"))
		   (hint te)
    end

end
