all: mlton bin/vmailpasswd elisp/domtool-tables.el

bin/vmailpasswd: src/mail/vmailpasswd.c
	$(CC) -lcrypt -o $@ $<

COMMON_DEPS := configDefault/config.sig configDefault/configDefault.sml \
	openssl/openssl_sml.so pcre/pcre_sml.so config.sml

EMACS_DIR := /usr/local/share/emacs/site-lisp/domtool-mode

config.sml:
	echo -e 'structure Config :> CONFIG = struct\nopen ConfigDefault\nend' > $@

.PHONY: all mlton smlnj install install_sos

mlton: bin/domtool-server bin/domtool-client bin/domtool-slave \
	bin/domtool-admin bin/domtool-doc bin/dbtool bin/vmail \
	bin/smtplog bin/setsa bin/mysql-fixperms bin/webbw

smlnj: $(COMMON_DEPS) openssl/smlnj/FFI/libssl.h.cm pcre/smlnj/FFI/libpcre.h.cm \
	src/domtool.cm

configDefault/config.sig: src/config.sig.header \
		configDefault/*.csg configDefault/*.cfs \
		src/config.sig.footer
	cat configDefault/*.csg \
		src/config.sig.header \
		configDefault/*.cfs \
		src/config.sig.footer \
		>configDefault/config.sig

configDefault/configDefault.sml: src/configDefault.sml.header \
		configDefault/*.cfg src/configDefault.sml.footer
	cat src/configDefault.sml.header \
		configDefault/*.cfg \
		src/configDefault.sml.footer \
		>configDefault/configDefault.sml

openssl/openssl_sml.o: openssl/openssl_sml.c
	gcc -fPIC -c openssl/openssl_sml.c -o openssl/openssl_sml.o

openssl/openssl_sml.so: openssl/openssl_sml.o
	gcc -shared -Wl,-soname,openssl_sml.so \
		-o openssl/openssl_sml.so \
		openssl/openssl_sml.o -lssl

pcre/pcre_sml.o: pcre/pcre_sml.c
	gcc -fPIC -c pcre/pcre_sml.c -o pcre/pcre_sml.o

pcre/pcre_sml.so: pcre/pcre_sml.o
	gcc -shared -Wl,-soname,pcre_sml.so \
		-o pcre/pcre_sml.so \
		pcre/pcre_sml.o -lpcre

src/domtool.cm: src/prefix.cm src/sources
	cat src/prefix.cm src/sources >src/domtool.cm

MAKE_MLB_BASE := cat src/prefix.mlb src/sources src/suffix.mlb \
	| sed 's/^\(.*\).grm$$/\1.grm.sig\n\1.grm.sml/' \
	| sed 's/^\(.*\).lex$$/\1.lex.sml/'

src/domtool-server.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/domtool-server.mlb
	echo "main-server.sml" >>src/domtool-server.mlb

src/domtool-client.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/domtool-client.mlb
	echo "main-client.sml" >>src/domtool-client.mlb

src/domtool-slave.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/domtool-slave.mlb
	echo "main-slave.sml" >>src/domtool-slave.mlb

src/domtool-admin.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/domtool-admin.mlb
	echo "main-admin.sml" >>src/domtool-admin.mlb

src/domtool-doc.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/domtool-doc.mlb
	echo "main-doc.sml" >>src/domtool-doc.mlb

src/dbtool.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/dbtool.mlb
	echo "main-dbtool.sml" >>src/dbtool.mlb

src/vmail.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/vmail.mlb
	echo "main-vmail.sml" >>src/vmail.mlb

src/setsa.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/setsa.mlb
	echo "main-setsa.sml" >>src/setsa.mlb

src/smtplog.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/smtplog.mlb
	echo "main-smtplog.sml" >>src/smtplog.mlb

src/mysql-fixperms.mlb: src/prefix.mlb src/sources src/suffix.mlb
	$(MAKE_MLB_BASE) >src/mysql-fixperms.mlb
	echo "main-mysql-fixperms.sml" >>src/mysql-fixperms.mlb

openssl/smlnj/FFI/libssl.h.cm: openssl/openssl_sml.h
	cd openssl/smlnj ; ml-nlffigen -d FFI -lh LibsslH.libh -include ../libssl-h.sml \
	-cm libssl.h.cm -D__builtin_va_list="void*" \
	../openssl_sml.h

openssl/mlton/FFI/libssl.h.mlb: openssl/openssl_sml.h
	cd openssl/mlton ; mlnlffigen -dir FFI -libhandle LibsslH.libh -include ../libssl-h.sml \
	-mlbfile libssl.h.mlb -cppopt -D__builtin_va_list="void*" \
	../openssl_sml.h

pcre/smlnj/FFI/libpcre.h.cm: pcre/pcre_sml.h
	cd pcre/smlnj ; ml-nlffigen -d FFI -lh LibpcreH.libh -include ../libpcre-h.sml \
	-cm libpcre.h.cm -D__builtin_va_list="void*" \
	../pcre_sml.h

pcre/mlton/FFI/libpcre.h.mlb: pcre/pcre_sml.h
	cd pcre/mlton ; mlnlffigen -dir FFI -libhandle LibpcreH.libh -include ../libpcre-h.sml \
	-mlbfile libpcre.h.mlb -cppopt -D__builtin_va_list="void*" \
	../pcre_sml.h

%.lex.sml: %.lex
	mllex $<

%.grm.sig %.grm.sml: %.grm
	mlyacc $<

COMMON_MLTON_DEPS := openssl/mlton/FFI/libssl.h.mlb \
	pcre/mlton/FFI/libpcre.h.mlb \
	src/domtool.lex.sml \
	src/domtool.grm.sig src/domtool.grm.sml \
	$(COMMON_DEPS) src/*.sig src/*.sml \
	src/plugins/*.sig src/plugins/*.sml \
	src/mail/*.sig src/mail/*.sml

MLTON := mlton -link-opt -ldl

ifdef DEBUG
	MLTON += -const 'Exn.keepHistory true'
endif

bin/domtool-server: $(COMMON_MLTON_DEPS) src/domtool-server.mlb 
	$(MLTON) -output bin/domtool-server src/domtool-server.mlb

bin/domtool-client: $(COMMON_MLTON_DEPS) src/domtool-client.mlb
	$(MLTON) -output bin/domtool-client src/domtool-client.mlb

bin/domtool-slave: $(COMMON_MLTON_DEPS) src/domtool-slave.mlb
	$(MLTON) -output bin/domtool-slave src/domtool-slave.mlb

bin/domtool-admin: $(COMMON_MLTON_DEPS) src/domtool-admin.mlb
	$(MLTON) -output bin/domtool-admin src/domtool-admin.mlb

bin/domtool-doc: $(COMMON_MLTON_DEPS) src/domtool-doc.mlb
	$(MLTON) -output bin/domtool-doc src/domtool-doc.mlb

bin/dbtool: $(COMMON_MLTON_DEPS) src/dbtool.mlb
	$(MLTON) -output bin/dbtool src/dbtool.mlb

bin/vmail: $(COMMON_MLTON_DEPS) src/vmail.mlb
	$(MLTON) -output bin/vmail src/vmail.mlb

bin/setsa: $(COMMON_MLTON_DEPS) src/setsa.mlb
	$(MLTON) -output bin/setsa src/setsa.mlb

bin/smtplog: $(COMMON_MLTON_DEPS) src/smtplog.mlb
	$(MLTON) -output bin/smtplog src/smtplog.mlb

bin/mysql-fixperms: $(COMMON_MLTON_DEPS) src/mysql-fixperms.mlb
	$(MLTON) -output bin/mysql-fixperms src/mysql-fixperms.mlb

bin/webbw: $(COMMON_MLTON_DEPS) src/stats/webbw.mlb
	mlton -output bin/webbw src/stats/webbw.mlb

elisp/domtool-tables.el: lib/*.dtl bin/domtool-doc
	bin/domtool-doc -basis -emacs >$@

install_sos:
	cp openssl/openssl_sml.so /usr/local/lib/
	cp pcre/pcre_sml.so /usr/local/lib/

install: install_sos
	cp scripts/domtool-publish /usr/local/sbin/
	cp scripts/domtool-reset-global /usr/local/sbin/
	cp scripts/domtool-reset-local /usr/local/sbin/
	cp scripts/domtool-adduser /usr/local/bin/
	cp scripts/domtool-addcert /usr/local/bin/
	cp scripts/domtool-addcert-daemon /usr/local/bin/
	cp scripts/domtool-addacl /usr/local/bin/
	cp scripts/domtool-rmuser /usr/local/bin/
	cp scripts/domtool-admin-sudo /usr/local/bin/
	cp scripts/domtool-server-logged /usr/local/bin/
	cp scripts/domtool-slave-logged /usr/local/bin/
	cp scripts/domtool-server /etc/init.d/
	cp scripts/domtool-slave /etc/init.d/
	-cp bin/domtool-server /usr/local/sbin/
	-cp bin/domtool-slave /usr/local/sbin/
	-cp bin/domtool-client /usr/local/bin/domtool
	-cp bin/domtool-admin /usr/local/bin/
	-cp bin/domtool-doc /usr/local/bin/
	-cp bin/dbtool /usr/local/bin/
	-cp bin/vmail /usr/local/bin/
	-cp bin/setsa /usr/local/bin/
	-cp bin/smtplog /usr/local/bin/
	-cp bin/mysql-fixperms /usr/local/bin/
	-cp bin/vmailpasswd /usr/local/bin/
	-cp bin/webbw /usr/local/sbin/
	cp src/plugins/domtool-postgres /usr/local/sbin/
	cp src/plugins/domtool-mysql /usr/local/sbin/
	-mkdir -p $(EMACS_DIR)
	cp elisp/*.el $(EMACS_DIR)/

.PHONY: grab_lib install_server install_slave

grab_lib:
	rsync -r --delete /afs/hcoop.net/common/etc/domtool/lib/* lib/

install_server:
	sudo /etc/init.d/domtool-server stop
	sudo make install
	sudo /etc/init.d/domtool-server start

install_slave:
	sudo /etc/init.d/domtool-slave stop
	sudo make install
	sudo /etc/init.d/domtool-slave start
