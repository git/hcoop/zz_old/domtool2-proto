structure LibpcreH = struct
    local
        val lh = DynLinkage.open_lib
             { name = "/usr/local/lib/pcre_sml.so", global = true, lazy = true }
	    handle DynLinkage.DynLinkError s => raise Fail s
    in
        fun libh s = let
            val sh = DynLinkage.lib_symbol (lh, s)
        in
            fn () => DynLinkage.addr sh
        end
    end
end
