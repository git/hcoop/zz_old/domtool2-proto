{{The most common kinds of domain configuration}}

val default_node : (node) = "mire";
val web_node : (web_node) = "mire";

val webAt =
  \ n : (web_node) ->
  \ host : (host) ->
  \\ config : Vhost -> begin
    dns (dnsA host (ip_of_node (web_node_to_node n)));

    vhost host where
      WebPlaces = [web_place_default n]
    with
      config
    end
  end;

val web = webAt web_node;

val webAtIp =
  \ ip : (your_ip) ->
  \ host : (host) ->
  \\ config : Vhost -> begin
    dns (dnsA host (your_ip_to_ip ip));

    vhost host where
      WebPlaces = [web_place web_node ip]
    with
      config
    end
  end;

val addDefaultAlias = begin
  mailbox <- Mailbox;
  defaultAlias mailbox
end;

val addWww = begin
  web "www" with
    serverAliasDefault;
    www : [Vhost] <- WWW;
    www
  end
end;

val dom =
  \ d : (your_domain) ->
  \\ config : Domain ->
  domain d with
    dns (dnsNS "ns1.hcoop.net");
    dns (dnsNS "ns3.hcoop.net");

    dns (dnsDefaultA (ip_of_node (web_node_to_node web_node)));

    handleMail;
    dns (dnsMX 1 "deleuze.hcoop.net");

    createWWW : bool <- CreateWWW;
    if createWWW then
      addWww
    else
      Skip
    end;

    defAl : bool <- DefaultAlias;
    if defAl then
      addDefaultAlias
    else
      Skip
    end;

    config
  end;

val nameserver = \host -> dns (dnsNS host);
val dnsIP = \from -> \to -> dns (dnsA from to);
val dnsIPv6 = \from -> \to -> dns (dnsAAAA from to);
val dnsMail = \num -> \host -> dns (dnsMX num host);
val dnsAlias = \from -> \to -> dns (dnsCNAME from to);
val dnsDefault = \to -> dns (dnsDefaultA to);
val dnsDefaultv6 = \to -> dns (dnsDefaultAAAA to);
