#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"

void OpenSSL_SML_init(void);
void OpenSSL_SML_load_error_strings(void);
void OpenSSL_SML_load_BIO_strings(void);

int OpenSSL_SML_get_error(void);
int OpenSSL_SML_should_retry(BIO *b);
const char *OpenSSL_SML_lib_error_string(int err);
const char *OpenSSL_SML_func_error_string(int err);
const char *OpenSSL_SML_reason_error_string(int err);

int OpenSSL_SML_read(BIO *b, void *data, int len);
int OpenSSL_SML_write(BIO *b, const void *data, int len);

BIO *OpenSSL_SML_new_connect(char *addr);
int OpenSSL_SML_do_connect(BIO *b);

int OpenSSL_SML_do_accept(BIO *b);

int OpenSSL_SML_do_handshake(BIO *b);

void OpenSSL_SML_free_all(BIO *b);

SSL_METHOD *OpenSSL_SML_SSLv23_method(void);

SSL_CTX *OpenSSL_SML_CTX_new(SSL_METHOD *meth);
void OpenSSL_SML_CTX_free(SSL_CTX *ctx);

int OpenSSL_SML_load_verify_locations(SSL_CTX *ctx, const char *trust, const char *certs);

BIO *OpenSSL_SML_new_ssl_connect(SSL_CTX *ctx);
SSL *OpenSSL_SML_get_ssl(BIO *bio);
int OpenSSL_SML_set_conn_hostname(BIO *bio, char *hostname);
int OpenSSL_SML_set_accept_port(BIO *bio, char *port);

int OpenSSL_SML_tcp_listen(int port, int qsize);
int OpenSSL_SML_accept(int sock);
BIO *OpenSSL_SML_new_socket(int sock);

SSL *OpenSSL_SML_SSL_new(SSL_CTX *ctx);

int OpenSSL_SML_SSL_shutdown(SSL *ssl);
void OpenSSL_SML_shutdown(int sock);
void OpenSSL_SML_SSL_set_bio(SSL *ssl, BIO *b1, BIO *b2);

int OpenSSL_SML_use_PrivateKey_file(SSL_CTX *ctx, char *keyfile);

int OpenSSL_SML_SSL_accept(SSL *ssl);

int OpenSSL_SML_use_certificate_chain_file(SSL_CTX *ctx, char *keyfile);

const char *OpenSSL_SML_get_peer_name(SSL *ssl);

BIO *OpenSSL_SML_new_ssl(SSL_CTX *ctx);
BIO *OpenSSL_SML_new_accept(SSL_CTX *ctx, char *port);

BIO *OpenSSL_SML_pop(BIO *b);
BIO *OpenSSL_SML_next(BIO *b);

int OpenSSL_SML_puts(BIO *b, const char *buf);
